﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MiroBozik.MessageBus.Test
{
    [TestClass]
    public class TopicWildcardTest
    {
        [TestMethod]
        public void IsMatch()
        {
            var wildcard = new TopicWildcard();
            
            IEnumerable<string> match;

            var stop = Stopwatch.StartNew();
            var isMatch = wildcard.IsMatch("var/test/one", "var/+/+", out match);
            stop.Stop();
            Assert.IsTrue(isMatch);

            Console.WriteLine("match: {0}", string.Join(",", match));
            Console.WriteLine("Elapsed: {0}", stop.Elapsed);
        }
    }
}