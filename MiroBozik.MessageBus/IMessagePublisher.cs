﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MiroBozik.MessageBus
{
    public interface IMessagePublisher
    {        
        Task PublishAsync(
            Type messageType,
            object message,
            CancellationToken cancellationToken = default(CancellationToken));       
    }
}