﻿using System;

namespace MiroBozik.MessageBus
{
    public interface IMessageBus : IMessagePublisher, IMessageSubscriber, IDisposable
    {        
    }
}