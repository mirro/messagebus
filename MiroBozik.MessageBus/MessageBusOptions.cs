﻿namespace MiroBozik.MessageBus
{
    public abstract class MessageBusOptions
    {
        public string Topic { get; set; }
    }
}