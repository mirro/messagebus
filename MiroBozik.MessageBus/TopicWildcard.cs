﻿using System.Collections.Generic;
using System.Linq;

namespace MiroBozik.MessageBus
{
    public class TopicWildcard
    {
        private readonly char _anyChar;
        private readonly char _any;

        public TopicWildcard(
            char anyChar = '#', 
            char any = '+')
        {
            _anyChar = anyChar;
            _any = any;
        }

        public bool IsMatch(string topicName, string topicFilter, out IEnumerable<string> match)
        {
            if (topicName == topicFilter)
            {
                match = Enumerable.Empty<string>();
                return true;
            } else if (topicFilter == ""+_anyChar) {
                match = new [] {topicName};
                return true;
            }

            var res = new List<string>();

            var t = topicName.Split('/');
            var w = topicFilter.Split('/');

            var i = 0;
            for (var lt = t.Length; i < lt; i++) {
                if (w[i] == ""+_any) {
                    res.Add(t[i]);
                } 
                else if (w[i] == ""+_anyChar)
                {
                    var m = string.Join("/", t.Skip(i));
                    res.Add(m);
                    match = res;
                    return true;
                } else if (w[i] != t[i]) {
                    match = null;
                    return false;
                }
            }

            if (w.Length < i && w[i] == ""+_anyChar) {
                i += 1;
            }

            if (i == w.Length)
            {
                match = res;
                return true;
            }
            match = null;
            return false;
        }        
    }
}