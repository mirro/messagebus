﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MiroBozik.MessageBus
{
    public static class MessageBusExtensions {        

        public static Task SubscribeAsync<T>(
            this IMessageSubscriber subscriber,             
            Func<T, Task> handler, 
            CancellationToken cancellationToken = default (CancellationToken)) 
            where T : class, new()
        {
            return subscriber.SubscribeAsync<T>(
                (msg, token) => handler(msg), 
                cancellationToken
                );
        }

        public static Task SubscribeAsync<T>(
            this IMessageSubscriber subscriber, 
            Action<T> handler, 
            CancellationToken cancellationToken = default(CancellationToken)
            ) 
            where T : class, new() {

            return subscriber.SubscribeAsync<T>((msg, token) => {
                handler(msg);
                return Task.CompletedTask;
            }, cancellationToken);
        }

        public static Task PublishAsync<T>(
            this IMessagePublisher publisher,
            T message,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return publisher.PublishAsync(typeof(T), message, cancellationToken);
        }
    }
}