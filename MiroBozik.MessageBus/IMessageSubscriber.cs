﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MiroBozik.MessageBus
{
    public interface IMessageSubscriber
    {                
        Task SubscribeAsync<T>(
            Func<T, CancellationToken, Task> handler,
            CancellationToken cancellationToken = default(CancellationToken))
            where T : class;        
    }
}