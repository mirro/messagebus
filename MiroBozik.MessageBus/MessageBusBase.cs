﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace MiroBozik.MessageBus
{
    public abstract class MessageBusBase<TOptions> : IMessageBus where TOptions : MessageBusOptions
    {
        protected readonly ConcurrentDictionary<string, Subscriber> _subscribers = new ConcurrentDictionary<string, Subscriber>();
        
        private TOptions _options;
        protected readonly ILogger _logger;
        private readonly ConcurrentQueue<Func<Task>> _queue = new ConcurrentQueue<Func<Task>>();
        private bool _sending;

        public MessageBusBase(TOptions options, ILoggerFactory loggerFactory)
        {
            _options = options;
            _logger = loggerFactory.CreateLogger<MessageBusBase<TOptions>>();
        }

        protected virtual Task EnsureTopicCreatedAsync(Type messageType, CancellationToken cancellationToken) => Task.CompletedTask;
        protected abstract Task PublishImplAsync(Type messageType, object message, CancellationToken cancellationToken);
        public async Task PublishAsync(Type messageType, object message, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (messageType == null)
            {
                throw new ArgumentNullException(nameof(messageType));
            }

            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            await EnsureTopicCreatedAsync(messageType, cancellationToken).ConfigureAwait(false);
            await PublishImplAsync(messageType, message, cancellationToken).ConfigureAwait(false);
        }

        protected virtual Task EnsureTopicSubscriptionAsync(Type type, CancellationToken cancellationToken) => Task.CompletedTask;
        protected virtual Task SubscribeImplAsync<T>(Func<T, CancellationToken, Task> handler, CancellationToken cancellationToken) where T : class {
            var subscriber = new Subscriber {
                CancellationToken = cancellationToken,
                Type = typeof(T),
                Action = (message, token) => {
                    if (!(message is T)) {
                        if (_logger.IsEnabled(LogLevel.Trace))
                            _logger.LogTrace("Unable to call subscriber action: {MessageType} cannot be safely casted to {SubscriberType}", message.GetType(), typeof(T));
                        return Task.CompletedTask;
                    }

                    return handler((T)message, cancellationToken);
                }
            };

            if (!_subscribers.TryAdd(subscriber.Id, subscriber) && _logger.IsEnabled(LogLevel.Error))
                _logger.LogError("Unable to add subscriber {SubscriberId}", subscriber.Id);

            return Task.CompletedTask;
        }
        public async Task SubscribeAsync<T>(Func<T, CancellationToken, Task> handler, CancellationToken cancellationToken = default(CancellationToken)) where T : class
        {
            if (_logger.IsEnabled(LogLevel.Trace))
                _logger.LogTrace("Adding subscriber for {MessageType}.", typeof(T).FullName);
            await EnsureTopicSubscriptionAsync(typeof(T), cancellationToken).ConfigureAwait(false);
            await SubscribeImplAsync(handler, cancellationToken).ConfigureAwait(false);
        }

        public void Dispose()
        {
            _logger.LogTrace("Disposing");                                    
            _subscribers.Clear();            
        }

        protected virtual Task SendMessageToSubscribers(IEnumerable<Subscriber> subscribers, Type messageType, object message)
        {
            bool isTraceLogLevelEnabled = _logger.IsEnabled(LogLevel.Trace);
            if (isTraceLogLevelEnabled)
            {
                _logger.LogTrace("Found {SubscriberCount} subscribers for message type {MessageType}.", subscribers.Count(), messageType.Name);
            }

            foreach (var subscriber in subscribers)
            {
                if (subscriber.CancellationToken.IsCancellationRequested) {
                    if (_subscribers.TryRemove(subscriber.Id, out var _)) {
                        if (isTraceLogLevelEnabled)
                            _logger.LogTrace("Removed cancelled subscriber: {SubscriberId}", subscriber.Id);
                    } else if (isTraceLogLevelEnabled) {
                        _logger.LogTrace("Unable to remove cancelled subscriber: {SubscriberId}", subscriber.Id);
                    }

                    continue;
                }

                _queue.Enqueue(async () =>
                {
                    if (subscriber.CancellationToken.IsCancellationRequested)
                    {
                        if (isTraceLogLevelEnabled)
                            _logger.LogTrace("The cancelled subscriber action will not be called: {SubscriberId}",
                                subscriber.Id);

                        return;
                    }

                    if (isTraceLogLevelEnabled)
                        _logger.LogTrace("Calling subscriber action: {SubscriberId}", subscriber.Id);

                    try
                    {
                        await subscriber.Action(message, subscriber.CancellationToken).ConfigureAwait(false);
                        if (isTraceLogLevelEnabled)
                            _logger.LogTrace("Finished calling subscriber action: {SubscriberId}", subscriber.Id);
                    }
                    catch (Exception ex)
                    {
                        if (_logger.IsEnabled(LogLevel.Warning))
                            _logger.LogWarning(ex, "Error sending message to subscriber: {Message}", ex.Message);
                    }
                });

                StartSending();
            }
            return Task.CompletedTask;
        }

        private void StartSending()
        {
            if (_sending)
            {
                return;
            }

            Task.Run(async () =>
            {
                _sending = true;
                _queue.TryDequeue(out var task);
                while (task != null)
                {
                    var t = task;
                    await Task.Run(() => t());

                    _queue.TryDequeue(out task);
                }
            })
            .ContinueWith((t) =>
            {
                _sending = false;
                return Task.CompletedTask;
            });
        }
    }
}