﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace MiroBozik.MessageBus.Internal
{
    public class InMemoryMessageBus : MessageBusBase<InMemoryMessageBusOptions>
    {
        public InMemoryMessageBus(IOptions<InMemoryMessageBusOptions> options, ILoggerFactory loggerFactory) : base(options.Value, loggerFactory)
        {
        }

        protected override Task PublishImplAsync(Type messageType, object message, CancellationToken cancellationToken)
        {
            if (_subscribers.IsEmpty)
            {
                return Task.CompletedTask;
            }

            bool isTraceLogLevelEnabled = _logger.IsEnabled(LogLevel.Trace);            

            var subscribers = _subscribers.Values.Where(s => s.IsAssignableFrom(messageType)).ToList();
            if (!subscribers.Any()) {
                if (isTraceLogLevelEnabled)
                {
                    _logger.LogTrace("Done sending message to 0 subscribers for message type {MessageType}.", messageType.Name);
                }

                return Task.CompletedTask;
            }

            if (isTraceLogLevelEnabled)
            {
                _logger.LogTrace("Message Publish: {MessageType}", messageType.FullName);
            }

            SendMessageToSubscribers(subscribers, messageType, message/*message.DeepClone()*/);
            return Task.CompletedTask;
        }
    }
}