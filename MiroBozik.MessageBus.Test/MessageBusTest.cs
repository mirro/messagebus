using System;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MiroBozik.MessageBus.Internal;

namespace MiroBozik.MessageBus.Test
{
    [TestClass]
    public class MessageBusTest
    {
        
        [TestMethod]
        public void Subscribe()
        {
            var options = Options.Create<InMemoryMessageBusOptions>(new InMemoryMessageBusOptions());
            
            var bus = new InMemoryMessageBus(options, new NullLoggerFactory());
            bus.SubscribeAsync<TestMessage>((msg) =>
            {
                Console.WriteLine("received message: {0}", msg.Text);

                Assert.IsNotNull(msg);
            })
            .GetAwaiter()
            .GetResult();

            bus.SubscribeAsync<Order>((msg) =>
                {
                    Console.WriteLine("received order: {0}", msg.Total);

                    Assert.IsNotNull(msg);
                })
                .GetAwaiter()
                .GetResult();            

            var message = new TestMessage {Text = "hello"};

            bus.PublishAsync(message)
                .GetAwaiter()
                .GetResult();

            var order = new Order {Total = 100};
            bus.PublishAsync(order)
                .GetAwaiter()
                .GetResult();
        }
    }

    class TestMessage
    {
        public string Text { get; set; }
    }

    class Order
    {
        public double Total { get; set; }
    }
}
