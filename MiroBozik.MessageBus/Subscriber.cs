﻿using System;
using System.Collections.Concurrent;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace MiroBozik.MessageBus
{
    public class Subscriber
    {
        private readonly ConcurrentDictionary<Type, bool> _assignableTypesCache = new ConcurrentDictionary<Type, bool>();

        public Subscriber()
        {
            Id = Guid.NewGuid().ToString();
        }

        public string Id { get; }

        public CancellationToken CancellationToken { get; set; }

        public Type Type { get; set; }
        
        public Func<object, CancellationToken, Task> Action { get; set; }

        public bool IsAssignableFrom(Type type) {
            return _assignableTypesCache.GetOrAdd(type, t => Type.GetTypeInfo().IsAssignableFrom(t));
        }
    }
}